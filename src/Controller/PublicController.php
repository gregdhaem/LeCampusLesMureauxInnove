<?php

namespace App\Controller;

use App\Entity\TArticles;
use App\Repository\TArticlesRepository;
use App\Repository\TEventsRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class PublicController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function home(TArticlesRepository $article, TEventsRepository $event)
    {
        /*  $articles = $repo ->findBy(
              array('title' => !null),       // Critère
              array('date' => 'desc'),      // Tri
              3,                           // Limite
              0                           // Offset
          ); A VOIR si ça marche pour la récupération et l'affichage des 3 derniers articles seulement*/

        $articles = $article ->findAll();
        $events = $event->findAll();
        return $this->render('public/home.html.twig', [
            'articles'=> $articles,
            'events'=> $events
        ]);
    }
    /**
     * @Route("/public/about_us", name="about_us")
     */
    public function about_us()
    {
        return $this->render('public/about_us.html.twig', [
            'controller_name' => 'about_us',
        ]);
    }
    /**
     * @Route("/public/events", name="events")
     */
    public function events(TEventsRepository $repo)
    {
        $events = $repo ->findAll();
        return $this->render('public/events.html.twig', [
            'events' => $events,

        ]);
    }
    /**
     * @Route("/public/articles", name="articles")
     */
    public function articles(TArticlesRepository $repo)
    {
        $articles = $repo ->findAll();
        return $this->render('public/articles.html.twig', [
            'articles' => $articles,
        ]);
    }

    /**
     * @Route("/public/article/{id}", name="article")
     */
    public function article(TArticles $article)
    {

        return $this->render('public/article_show.html.twig', [
            'article' => $article,
        ]);
    }


    /**
     * @Route("/public/find_us", name="find_us")
     */
    public function find_us()
    {
        return $this->render('public/find_us.html.twig', [
            'controller_name' => 'Find_us',
        ]);
    }

    /**
     * @Route("/public/terms_conditions", name="terms_conditions")
     */
    public function terms_conditions()
    {
        return $this->render('public/terms_conditions.html.twig', [
            'controller_name' => 'terms_conditions',
        ]);
    }
}

