<?php

namespace App\Controller;

use App\Entity\UserAdmin;
use App\Form\AdministrationType;

use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminSecurityController extends Controller
{
    /**
     * @Route("/admin/registration", name="admin_registration")
     */
    public function adminRegistration(Request $request, ObjectManager $manager, UserPasswordEncoderInterface $encoder)
    {
        $user_admin = new UserAdmin();

        $form = $this->createForm(AdministrationType::class, $user_admin);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $hash = $encoder->encodePassword($user_admin, $user_admin->getPassword());
            $user_admin->setPassword($hash);

            $manager->persist($user_admin);
            $manager->flush();

            return $this->redirectToRoute('admin_login');
        }

        return $this->render('admin_security/registration.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/admin", name="admin_login")
     */
    public function adminLogin()
    {
        return $this->render('admin_security/login.html.twig');
        return $this->redirectToRoute('admin');
    }

    /**
     * @Route("/adminlogout", name="admin_logout")
     */
    public function adminLogout()
    {

    }
}
