<?php

namespace App\Repository;

use App\Entity\TArticles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TArticles|null find($id, $lockMode = null, $lockVersion = null)
 * @method TArticles|null findOneBy(array $criteria, array $orderBy = null)
 * @method TArticles[]    findAll()
 * @method TArticles[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TArticlesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TArticles::class);
    }

//    /**
//     * @return TArticles[] Returns an array of TArticles objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TArticles
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
