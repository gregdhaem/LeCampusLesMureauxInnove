<?php

namespace App\Repository;

use App\Entity\TArticlesHasEvents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TArticlesHasEvents|null find($id, $lockMode = null, $lockVersion = null)
 * @method TArticlesHasEvents|null findOneBy(array $criteria, array $orderBy = null)
 * @method TArticlesHasEvents[]    findAll()
 * @method TArticlesHasEvents[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TArticlesHasEventsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TArticlesHasEvents::class);
    }

//    /**
//     * @return TArticlesHasEvents[] Returns an array of TArticlesHasEvents objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TArticlesHasEvents
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
