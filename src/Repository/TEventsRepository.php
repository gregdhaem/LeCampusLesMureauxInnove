<?php

namespace App\Repository;

use App\Entity\TEvents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TEvents|null find($id, $lockMode = null, $lockVersion = null)
 * @method TEvents|null findOneBy(array $criteria, array $orderBy = null)
 * @method TEvents[]    findAll()
 * @method TEvents[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TEventsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TEvents::class);
    }

//    /**
//     * @return TEvents[] Returns an array of TEvents objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TEvents
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
