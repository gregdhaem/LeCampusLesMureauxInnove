<?php

namespace App\Repository;

use App\Entity\TUsersHasTEvents;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method TUsersHasTEvents|null find($id, $lockMode = null, $lockVersion = null)
 * @method TUsersHasTEvents|null findOneBy(array $criteria, array $orderBy = null)
 * @method TUsersHasTEvents[]    findAll()
 * @method TUsersHasTEvents[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TUsersHasTEventsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, TUsersHasTEvents::class);
    }

//    /**
//     * @return TUsersHasTEvents[] Returns an array of TUsersHasTEvents objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TUsersHasTEvents
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
