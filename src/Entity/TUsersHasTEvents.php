<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TUsersHasTEventsRepository")
 */
class TUsersHasTEvents
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contacted;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contacted_via;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TUsers", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $tusers_id_users;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TEvents", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $tevents_id_events;

    public function getId()
    {
        return $this->id;
    }

    public function getContacted(): ?string
    {
        return $this->contacted;
    }

    public function setContacted(string $contacted): self
    {
        $this->contacted = $contacted;

        return $this;
    }

    public function getContactedVia(): ?string
    {
        return $this->contacted_via;
    }

    public function setContactedVia(string $contacted_via): self
    {
        $this->contacted_via = $contacted_via;

        return $this;
    }

    public function getTusersIdUsers(): ?TUsers
    {
        return $this->tusers_id_users;
    }

    public function setTusersIdUsers(TUsers $tusers_id_users): self
    {
        $this->tusers_id_users = $tusers_id_users;

        return $this;
    }

    public function getTeventsIdEvents(): ?TEvents
    {
        return $this->tevents_id_events;
    }

    public function setTeventsIdEvents(TEvents $tevents_id_events): self
    {
        $this->tevents_id_events = $tevents_id_events;

        return $this;
    }
}
