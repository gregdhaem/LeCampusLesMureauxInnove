<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TUsersRepository")
 */
class TUsers
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $newsletter;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $doc_request;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $sexe;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $company;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $direct_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cell_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $job_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $badge;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;

    public function getId()
    {
        return $this->id;
    }

    public function getNewsletter(): ?string
    {
        return $this->newsletter;
    }

    public function setNewsletter(string $newsletter): self
    {
        $this->newsletter = $newsletter;

        return $this;
    }

    public function getDocRequest(): ?string
    {
        return $this->doc_request;
    }

    public function setDocRequest(string $doc_request): self
    {
        $this->doc_request = $doc_request;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getDirectPhone(): ?string
    {
        return $this->direct_phone;
    }

    public function setDirectPhone(?string $direct_phone): self
    {
        $this->direct_phone = $direct_phone;

        return $this;
    }

    public function getCellPhone(): ?string
    {
        return $this->cell_phone;
    }

    public function setCellPhone(?string $cell_phone): self
    {
        $this->cell_phone = $cell_phone;

        return $this;
    }

    public function getJobTitle(): ?string
    {
        return $this->job_title;
    }

    public function setJobTitle(string $job_title): self
    {
        $this->job_title = $job_title;

        return $this;
    }

    public function getBadge(): ?string
    {
        return $this->badge;
    }

    public function setBadge(?string $badge): self
    {
        $this->badge = $badge;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(?string $password): self
    {
        $this->password = $password;

        return $this;
    }
}
